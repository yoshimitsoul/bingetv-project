DROP DATABASE IF EXISTS myNetflix;
CREATE DATABASE myNetflix;
USE myNetflix;

CREATE TABLE public(
id int NOT NULL,
nom varchar(50) NOT NULL,
CONSTRAINT pk_public PRIMARY KEY(id)
);

CREATE TABLE episode(
id int NOT NULL,
titre varchar(150)NOT NULL,
titreOriginal varchar(150)NOT NULL,
anneeParution date NOT NULL,
resume text NOT NULL,
idpublic int NOT NULL,
CONSTRAINT pk_episode PRIMARY KEY(id),
CONSTRAINT fk_public FOREIGN KEY (idpublic) REFERENCES public(id)
);

CREATE TABLE saisons(
id int NOT NULL,
resume text NOT NULL,
anneeReal date NOT NULL,
idepisode int NOT NULL,
CONSTRAINT pk_saisons PRIMARY KEY(id),
CONSTRAINT fk_episode FOREIGN KEY (idepisode) REFERENCES episode(id)
);

CREATE TABLE genres(
id int NOT NULL,
nom varchar(50) NOT NULL,
CONSTRAINT pk_genres PRIMARY KEY(id)
);

CREATE TABLE series(
id int NOT NULL,
nom varchar(50) NOT NULL,
nomoriginal varchar(100) NOT NULL,
anneeParution date NOT NULL,
resume text NOT NULL,
idgenre int NOT NULL,
CONSTRAINT pk_series PRIMARY KEY(id),
CONSTRAINT fk_genre FOREIGN KEY (idgenre) REFERENCES genre(id)
);

CREATE TABLE langue(
id int NOT NULL,
nom varchar(50) NOT NULL,
CONSTRAINT pk_langue PRIMARY KEY(id)
);

CREATE TABLE sousTitre(
idEpisode int NOT NULL,
idLangue int NOT NULL,
CONSTRAINT pk_sousTitre PRIMARY KEY(idEpisode, idlangue),
CONSTRAINT fk_langue FOREIGN KEY (idLangue) REFERENCES langue(id),
CONSTRAINT fk_episode FOREIGN KEY (idEpisode) REFERENCES episode(id)
);


CREATE TABLE audio(
idEpisode int NOT NULL,
idLangue int NOT NULL,
CONSTRAINT pk_audio PRIMARY KEY(idEpisode, idlangue),
CONSTRAINT fk_langue FOREIGN KEY (idLangue) REFERENCES langue(id),
CONSTRAINT fk_episode FOREIGN KEY (idEpisode) REFERENCES episode(id)
);

CREATE TABLE plateforme(
id int NOT NULL,
nom varchar(50) NOT NULL,
CONSTRAINT pk_plateforme PRIMARY KEY(id)
);

CREATE TABLE trailer(
idEpisode int NOT NULL,
idPlateforme int NOT NULL,
nom varchar(150) NOT NULL,
pays varchar(50) NOT NULL,
url varchar(250) NOT NULL,
qualite varchar(50) NOT NULL,
CONSTRAINT pk_trailer PRIMARY KEY(idEpisode, idPlateforme),
CONSTRAINT fk_episode FOREIGN KEY (idEpisode) REFERENCES episode(id),
CONSTRAINT fk_plateforme FOREIGN KEY (idPlateforme) REFERENCES plateforme(id)
);

CREATE TABLE stream(
idEpisode int NOT NULL,
idPlateforme int NOT NULL,
nom varchar(150) NOT NULL,
url varchar(250) NOT NULL,
CONSTRAINT pk_stream PRIMARY KEY(idEpisode, idPlateforme),
CONSTRAINT fk_episode FOREIGN KEY (idEpisode) REFERENCES episode(id),
CONSTRAINT fk_plateforme FOREIGN KEY (idPlateforme) REFERENCES plateforme(id)
);

CREATE TABLE qualite(
id int NOT NULL,
nom varchar(50) NOT NULL,
CONSTRAINT pk_qualite PRIMARY KEY(id)
);

CREATE TABLE pays(
id int NOT NULL,
nom varchar(50) NOT NULL,
idLangue int NOT NULL,
CONSTRAINT pk_pays PRIMARY KEY(id),
CONSTRAINT fk_langue FOREIGN KEY (idLangue) REFERENCES langue(id)
);

CREATE TABLE personne(
id int NOT NULL,
nom varchar(150) NOT NULL,
prenom varchar(150) NOT NULL,
dateNaissance date NOT NULL,
CONSTRAINT pk_personne PRIMARY KEY(id)
);

CREATE TABLE origine(
idpays int NOT NULL ,
idpersonne int NOT NULL,
origine boolean NOT NULL,
CONSTRAINT pk_origine PRIMARY KEY(idpays, idpersonne),
CONSTRAINT fk_pays FOREIGN KEY (idpays) REFERENCES pays(id),
CONSTRAINT fk_personne FOREIGN KEY (idpersonne) REFERENCES personne(id)
);

CREATE TABLE fonctions(
id int NOT NULL,
nom varchar(50) NOT NULL,
CONSTRAINT pk_fonction PRIMARY KEY(id)
);

CREATE TABLE roles(
idpersonne int NOT NULL,
idfonction int NOT NULL,
idepisode int NOT NULL,
CONSTRAINT pk_roles PRIMARY KEY(idpersonne, idfonction,idepisode ),
CONSTRAINT fk_fonction FOREIGN KEY (idfonction) REFERENCES fonctions(id),
CONSTRAINT fk_personne FOREIGN KEY (idpersonne) REFERENCES personne(id),
CONSTRAINT fk_episode FOREIGN KEY (idepisode) REFERENCES episode(id)
);


