function remplitCbJour(field){
    for (var i = 1; i <= 31; i++) {
        var newOption = new Option(i);
        field.options.add(newOption);
    }
}

function remplitCbMois(field){
    //incrementation des mois
    var tab = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'novembre', 'decembre'];
    for (var i = 0; i < 12; i++) {
        var newOption = new Option(tab[i]);
        field.options.add(newOption);
    }
}

function remplitCbAnnees(field){
    //inc années
    for (var i = 2049; i >= 1929; i--) {
        var newOption = new Option(i);
        field.options.add(newOption);
    }
 }

 function remplitCbRealisateurs(field) {
    //var xmlHttpFichier = new XMLHttpRequest();
    //xmlHttpFichier.onreadystatechange = function () {
    var xmlHttp = getAjaxRequestObject();
    xmlHttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("Jai mon fichier, je vais le traiter.");
            var lesRealisateurs = JSON.parse(this.responseText);
            lesRealisateurs.sort(function(a,b){
                return a.nom +" "+ a.prenom > b.nom +""+b.prenom ? 1: -1
            });
            console.log(lesRealisateurs);
            for (let index = 0; index < lesRealisateurs.length; index++) {
                var id = lesRealisateurs[index].id;
                var nom = lesRealisateurs[index].nom;
                var prenom = lesRealisateurs[index].prenom;
                field.options.add(new Option(nom + " " + prenom, id));
            }
        }
    };
    xmlHttp.open("GET", "https://www.devatom.net/formation/UDEV3/APINetflix/api.php?data=personnes&idfonction=4");
    xmlHttp.send();

}
