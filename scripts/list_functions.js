function getListeSeries(table){
    var xmlHttpSeries =getAjaxRequestObject(); 
    xmlHttpSeries.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
            var text =this.reponseText;
            var lesSeries = JSON.parse(this.responseText);
            lesSeries.sort(function(a, b){
                return a.nom > b.nom;
            });

            for(var i=0; i<lesSeries.length; i++){
                var row = table.insertRow(i);
                var cellnom = row.insertCell(0);
                var cellannee = row.insertCell(1);
                var celleSaisons = row.insertCell(2);
                cellnom.innerText = lesSeries[i].nom;
                cellannee.innerText =lesSeries[i].anneeparution;
                getNbSaisons(lesSeries[i].id, celleSaisons);
                row.setAttribute("tag", lesSeries[i].id);
                cellnom.style.textAlign ="left";
                cellnom.style.paddingLeft = "10px"; 

                row.onclick = function(){
                    console.log(this.Attribute ="tag")
                }
            }
        }
    };
    xmlHttpSeries.open("GET", "https://www.devatom.net/formation/UDEV3/APINetflix/api.php?data=series");
    xmlHttpSeries.send();
}

function getNbSaisons(id, cellule){
    var nbSaisons = 0;
    var xmlHttpSaisons = getAjaxRequestObject();
    xmlHttpSaisons.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
            try{
                nbSaisons = JSON.parse(this.responseText).length;
            }catch (e){
                nbSaisons = 0;
            }
            cellule.innerText = nbSaisons;
        }
    };
    xmlHttpSaisons.open("GET", "https://www.devatom.net/formation/UDEV3/APINetflix/api.php?data=saisons&idserie="+ id);
    xmlHttpSaisons.send();
    return nbSaisons;
}
