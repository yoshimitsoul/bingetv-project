document.body.onload =function(){
    var cbJ = document.querySelectorAll("select[name=cbJour]")[0];
    var cbA = document.querySelectorAll("select[name=cbAnnee]")[0];
    var cbM = document.querySelectorAll("select[name=cbMois]")[0];
    var cbRealisateurs = document.querySelectorAll("select[name=cbRealisateur]")[0];
    remplitCbJour(cbJ);
    remplitCbMois(cbM);
    remplitCbAnnees(cbA);
    remplitCbRealisateurs(cbRealisateurs);

}

    function ajoutReal() {
        var maListe = document.getElementsByName("cbRealisateur")[0];
        var newReal = prompt("ajouter");

        var exists = false;
        var i = 0;
        do {

            if (newReal === maListe.options[i].text) {
                exists = true;
            }
            i++;
        } while (!exists && i < maListe.options.length);
        if (!exists) {
            maListe.options.add(new Option(newReal, maListe.options.length));
        } else {
            // alert(newReal + " existe déjà"); affiche le nom déjà tapé
            i--;
        }
        maListe.options[i].selected = true;

    }

    function verifForm() {
            var champTitre = document.getElementsByName("ttTitre")[0];
            if (champTitre.value.trim( ) !== "") {
            }else {
                alert("Saisissez un titre");
                champTitre.focus();
                return false;
            }

            var cbJour = document.getElementsByName("cbJour")[0];
            if (cbJour.selectedIndex === 0){
                return false;
            }

            var cbMois = document.getElementsByName("cbMois")[0];
            if (cbMois.selectedIndex === 0){
                alert("Entrer un mois");
                return false;
            }
            var cbAnnee = document.getElementsByName("cbAnnee")[0];
            if (cbAnnee.selectedIndex === 0){
                alert("Entrer une année");
                return false;
            }

            var champReal = document.getElementsByName("cbRealisateur")[0];
            if (champReal.selectedIndex === 0) {
                alert("Entrez un réalisateur");
                return false;
            }

            if (!oneChecked()){
                alert("cochez un genre !")
                return false;
            }
            alert ("votre formulaire est complet et va être envoyé");
            return true ;

            function oneChecked(){
                var checkboxes = document.querySelectorAll("#checkGenres input[type=checkbox]") /* selection de tout les champs de type checkbox*/
                var checked = false; 
                var i =0;
                do{
                    checked = checkboxes[i].checked;
                    i++
                }while(!checked && i < checkboxes.length);
                return checked;
            }
        }

        function verifTitre(champ){
            if (champ.value.trim( ) !== "") {
                champ.style.borderColor = "green";
            }else {
                alert("Saisissez un titre");
                champ.style.borderColor = "red";
                champ.focus();
                return false;
            }
        }

        function verifSaison(champSaison){
            if(champSaison.value === ""){
                champSaison.style.borderColor = "red"; 
            } else  {
                champSaison.style.borderColor = "green";
                champ.focus();
                return false;
            }
        }

        function verifJour(champJour){
            if(champJour.value ==='Jour'){
                champJour.style.borderColor ="red";
            }else {
                champJour.style.borderColor="green";
                champJour.focus();
                return false; 
            }
        }

        function verifMois(champMois){
            if(champMois.value ==='Mois'){
                champMois.style.borderColor ="red";
            }else {
                champMois.style.borderColor="green";
                champMois.focus();
                return false; 
            }
        }

        function verifAn(champAnnee){
            if(champAnnee.value ==='Jour'){
                champAnnee.style.borderColor ="red";
            }else {
                champAnnee.style.borderColor="green";
                champAnnee.focus();
                return false;
            }
        }
